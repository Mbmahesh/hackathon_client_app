// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.factory('LoaderService', function($rootScope, $ionicLoading) {
  return {
        show : function() {

            $rootScope.loading = $ionicLoading.show({

              // The text to display in the loading indicator
              content: '<i class="icon ion-looping"></i> Loading',

              // The animation to use
              animation: 'fade-in',

              // Will a dark overlay or backdrop cover the entire view
              showBackdrop: true,

              // The maximum width of the loading indicator
              // Text will be wrapped if longer than maxWidth
              maxWidth: 200,

              // The delay in showing the indicator
              showDelay: 10
            });
        },

        hide : function(){
            $rootScope.loading.hide();
        }
    }
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })

    .state('app.SavingAccountSummary', {
      url: "/SavingAccountSummary",
    views: {
        'menuContent' :{
          templateUrl: "templates/SavingAccountSummary.html",
          controller: 'SavingAccountSummaryCtrl'
        }
    }
      
    })

    .state('app.CheckBalanceAccount', {
      url: "/CheckBalanceAccount",
    views: {
        'menuContent' :{
          templateUrl: "templates/CheckBalance.html",
          controller: 'CheckBalanceAccountCtrl'
        }
    }
      
    })

    .state('app.SavingsBalanceAccount', {
      url: "/SavingsBalanceAccount",
    views: {
        'menuContent' :{
          templateUrl: "templates/SavingsBalance.html",
          controller: 'SavingsBalanceAccountCtrl'
        }
    }
      
    })

    .state('app.DepositCheque', {
      url: "/DepositCheque",
     views: {
        'menuContent' :{
          templateUrl: "templates/ShowDeposits.html",
          controller: 'DepositChequeCtrl'
        }
    }
      
    })

     .state('app.AccountBalance', {
      url: "/ShowAccountBalance",
      views: {
        'menuContent' :{
          templateUrl: "templates/ShowAccountBalance.html",
          controller: 'AccountBalanceCtrl'
        }
      }
    })

    .state('app.Home', {
      url: "/Home",
    views: {
        'menuContent' :{
          templateUrl: "templates/Home.html",
          controller: 'AppCtrl'
        }
    }
      
    })

    .state('app.login', {
      url: "/login",
    views: {
        'menuContent' :{
          templateUrl: "templates/login.html",
          controller: 'AppCtrl'
        }
    }
      
    })

    .state('app.AccountSummary', {
      url: "/AccountSummary",
      views: {
        'menuContent' :{
          templateUrl: "templates/AccountSummary.html",
          controller: 'AccountSummaryCtrl'
        }
      }
    })

    .state('app.CheckAccountSummary', {
      url: "/CheckAccountSummary",
    views: {
        'menuContent' :{
          templateUrl: "templates/CheckAccountSummary.html",
          controller: 'CheckAccountSummaryCtrl'
        }
    }
      
    })

    .state('app.FundTransfer', {
      url: "/FundTransfer",
      views: {
        'menuContent' :{
          templateUrl: "templates/FundTransfer.html",
          controller: 'FundTransferCtrl'
        }
      }
    })

    .state('app.VoiceInput', {
      url: "/VoiceInput",
    views: {
        'menuContent' :{
          templateUrl: "templates/VoiceInput.html",
          controller: 'VoiceInputCtrl'
        }
    }
      
    })


    .state('app.CheckingToSavingsFundTransfer', {
      url: "/CheckingToSavingsFundTransfer",
      views: {
        'menuContent' :{
          templateUrl: "templates/CheckingToSavingsFundTransfer.html",
          controller: 'CheckingToSavingsFundTransferCtrl'
        }
      }
    })
  
  .state('app.SavingToCheckingFundTransfer', {
      url: "/SavingToCheckingFundTransfer",
      views: {
        'menuContent' :{
          templateUrl: "templates/SavingToCheckingFundTransfer.html",
          controller: 'SavingToCheckingFundTransferCtrl'
        }
      }
    })
  
  .state('app.FundTransferCheckingToThirdParty', {
      url: "/FundTransferCheckingToThirdParty",
      views: {
        'menuContent' :{
          templateUrl: "templates/FundTransferCheckingToThirdParty.html",
          controller: 'FundTransferCheckingToThirdPartyCtrl'
        }
      }
    })
  
  .state('app.FundTransferSavingsToThirdParty', {
      url: "/FundTransferSavingsToThirdParty",
      views: {
        'menuContent' :{
          templateUrl: "templates/FundTransferSavingsToThirdParty.html",
          controller: 'FundTransferSavingsToThirdPartyCtrl'
        }
      }
    })

  .state('app.UtilityBills', {
      url: "/UtilityBills",
    views: {
        'menuContent' :{
          templateUrl: "templates/UtilityBills.html",
          controller: 'UtilityBillsCtrl'
        }
    }
      
    })

   .state('app.ElectricityBill', {
      url: "/ElectricityBill",
      views: {
        'menuContent' :{
          templateUrl: "templates/ElectricityBill.html",
          controller: 'ElectricityBillCtrl'
        }
      }
    })
  
  .state('app.InternetBill', {
      url: "/InternetBill",
      views: {
        'menuContent' :{
          templateUrl: "templates/InternetBill.html",
          controller: 'InternetBillCtrl'
        }
      }
    })
  
   .state('app.CreditBill', {
      url: "/CreditBill",
      views: {
        'menuContent' :{
          templateUrl: "templates/CreditBill.html",
          controller: 'CreditBillCtrl'
        }
      }
    })
  
   .state('app.TelephoneBill', {
      url: "/TelephoneBill",
      views: {
        'menuContent' :{
          templateUrl: "templates/TelephoneBill.html",
          controller: 'TelephoneBillCtrl'
        }
      }
    })

   
   .state('app.FixedDeposits', {
      url: "/FixedDeposits",
      views: {
        'menuContent' :{
          templateUrl: "templates/FixedDeposits.html",
       controller: 'FixedCtrl'
        }
      }
    })
   .state('app.RecurringDeposits', {
      url: "/RecurringDeposits",
      views: {
        'menuContent' :{
          templateUrl: "templates/RecurringDeposits.html",
       controller: 'RecurringCtrl'
        }
      }
    })

   .state('app.NearestAtm', {
      url: "/NearestAtm",
    views: {
        'menuContent' :{
          templateUrl: "templates/NearestAtm.html",
          controller: 'NearestAtmCtrl'
        }
    }
      
    });
  
  
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});

